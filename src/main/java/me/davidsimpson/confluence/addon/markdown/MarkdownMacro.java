package me.davidsimpson.confluence.addon.markdown;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.webresource.WebResourceManager;
import me.davidsimpson.confluence.addon.markdown.manager.MarkdownSecurityManager;
import org.apache.commons.lang.StringEscapeUtils;
import org.pegdown.PegDownProcessor;

import java.util.Map;

/**
 * User: david
 * Date: 20/06/2013
 * Time: 20:08
 */
public class MarkdownMacro implements Macro
{
    private MarkdownSecurityManager markdownSecurityManager;
    private WebResourceManager webResourceManager;

    public MarkdownMacro(MarkdownSecurityManager markdownSecurityManager,
                         WebResourceManager webResourceManager)
    {
        this.markdownSecurityManager = markdownSecurityManager;
        this.webResourceManager = webResourceManager;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    @Override
    public String execute(Map<String, String> parameters,
                          String body,
                          ConversionContext conversionContext)
            throws MacroExecutionException
    {
        webResourceManager.requireResource("me.davidsimpson.confluence.addon.markdown-for-confluence:md-styles");
        body = markdownToHtml(body);
        return "<div class=\"markdown-macro\">" + body + "</div>";
    }

    private String markdownToHtml(String markdown)
    {
        if (markdownSecurityManager.getSecurity().isEscapeHtml() == true)
        {
           markdown = StringEscapeUtils.escapeHtml(markdown);
        }
        
        // New processor each time because pegdown is not thread-safe internally
        PegDownProcessor processor = new PegDownProcessor();
        return processor.markdownToHtml(markdown);
    }
}