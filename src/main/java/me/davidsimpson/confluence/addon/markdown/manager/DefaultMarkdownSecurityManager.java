package me.davidsimpson.confluence.addon.markdown.manager;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.thoughtworks.xstream.XStream;
import me.davidsimpson.confluence.addon.markdown.bean.MarkdownSecurity;
import org.apache.log4j.Logger;

/**
 * User: david
 * Date: 09/09/2013
 */
public class DefaultMarkdownSecurityManager implements MarkdownSecurityManager
{

    private static final Logger LOG = Logger.getLogger(DefaultMarkdownSecurityManager.class);

    private PluginSettingsFactory pluginSettingsFactory;

    private XStream xStream;

    public DefaultMarkdownSecurityManager() {
        xStream = new XStream();
        xStream.setClassLoader(DefaultMarkdownSecurityManager.class.getClassLoader());
    }

    @Override
    public void setSecurity(MarkdownSecurity markdownSecurity)
    {
        String values = xStream.toXML(markdownSecurity);
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        pluginSettings.put(ADD_ON_KEY, values);
    }

    @Override
    public MarkdownSecurity getSecurity()
    {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        Object value = pluginSettings.get(ADD_ON_KEY);

        if (value != null)
        {
            Object xml = xStream.fromXML(value.toString());

            if (xml instanceof MarkdownSecurity)
            {
                return (MarkdownSecurity) xml;
            }
        }

        MarkdownSecurity settings = new MarkdownSecurity();
        settings.setEscapeHtml(true); // Default to true.
        return settings;
    }

    public void setPluginSettingsFactory(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }
}
