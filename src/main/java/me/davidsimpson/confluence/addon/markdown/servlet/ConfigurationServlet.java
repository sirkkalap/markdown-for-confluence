package me.davidsimpson.confluence.addon.markdown.servlet;

import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.sal.api.auth.LoginUriProvider;

import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.user.User;
import me.davidsimpson.confluence.addon.markdown.MarkdownMacro;
import me.davidsimpson.confluence.addon.markdown.bean.MarkdownSecurity;
import me.davidsimpson.confluence.addon.markdown.manager.MarkdownSecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationServlet extends HttpServlet
{
    private final String template = "Confluence.Markdown.Configure.doGet";

    private static final Logger log = LoggerFactory.getLogger(ConfigurationServlet.class);

    private LoginUriProvider loginUriProvider;

    private MarkdownSecurityManager markdownSecurityManager;

    private PermissionManager permissionManager;

    private SoyTemplateRenderer soyTemplateRenderer;

    public ConfigurationServlet(LoginUriProvider loginUriProvider,
                                MarkdownSecurityManager markdownSecurityManager,
                                PermissionManager permissionManager,
                                SoyTemplateRenderer soyTemplateRenderer)
    {
        this.loginUriProvider = loginUriProvider;
        this.markdownSecurityManager = markdownSecurityManager;
        this.permissionManager = permissionManager;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        enforcePermissions(req, resp);
        resp.setContentType("text/html;charset=UTF-8");

        Map<String,Object> context = new HashMap<String,Object>();

        if (markdownSecurityManager.getSecurity().isEscapeHtml())
        {
            context.put("escapeHtmlTrue", "checked");
            context.put("escapeHtmlFalse", "");
        }
        else
        {
            context.put("escapeHtmlTrue", "");
            context.put("escapeHtmlFalse", "checked");
        }

        render(resp, template, context);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        enforcePermissions(req, resp);
        resp.setContentType("text/html;charset=UTF-8");

        Map<String,Object> context = new HashMap<String,Object>();
        MarkdownSecurity settings = new MarkdownSecurity();

        if (req.getParameter("escapeHtml").equals("true"))
        {
            settings.setEscapeHtml(true);
            context.put("escapeHtmlTrue", "checked");
            context.put("escapeHtmlFalse", "");
        }
        else
        {
            settings.setEscapeHtml(false);
            context.put("escapeHtmlTrue", "");
            context.put("escapeHtmlFalse", "checked");
        }

        markdownSecurityManager.setSecurity(settings);
        render(resp, template, context);
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data)
    {
        String webResource = "me.davidsimpson.confluence.addon.markdown-for-confluence:configure-soy";
        try
        {
            soyTemplateRenderer.render(resp.getWriter(), webResource, templateName, data);
        }
        catch (Exception e)
        {
            log.error("An error occurred in ConfigurationServlet.render(...) ", e);
        }
    }

    private void enforcePermissions(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        User user = AuthenticatedUserThreadLocal.getUser();

        if (user == null)
        {
            resp.sendRedirect(loginUriProvider.getLoginUri(URI.create(req.getRequestURL().toString())).toASCIIString());
            return;
        }
        else if (! permissionManager.isConfluenceAdministrator(user))
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
    }
}