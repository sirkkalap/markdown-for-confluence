package me.davidsimpson.confluence.addon.markdown.bean;

/**
 * User: david
 * Date: 09/09/2013
 */
public class MarkdownSecurity
{
    private boolean escapeHtml;

    public boolean isEscapeHtml() {
        return escapeHtml;
    }

    public void setEscapeHtml(boolean escapeHtml) {
        this.escapeHtml = escapeHtml;
    }
}
